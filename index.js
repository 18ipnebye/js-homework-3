"use strict";

/*1.
    Попросіть користувача ввести свій вік за допомогою prompt.
    Якщо вік менше 12 років, виведіть у вікні alert повідомлення про те,
    що він є дитиною, якщо вік менше 18 років, виведіть повідомлення про те,
    що він є підлітком, інакше виведіть повідомлення про те, що він є дорослим.
    Завдання з підвищенною складністю.
    Перевірте чи ввів користувач саме число в поле prompt. Якщо ні, то виведіть повідомлення, що введено не число.
 */

let userAge = +prompt("Enter your age -> ");

if (Number.isInteger(userAge) && userAge >= 0 && userAge <= 110) {

    console.log(userAge);

    if (userAge <= 12) {
        alert("You are a child");
    }
    else if (userAge <= 18){
        alert("You are a teenager");
    }else {
        alert("You are an adult");
    }

} else {
    alert("You did not enter a number or Wrong numbers :( ");
}

alert("Next task --------------------------------------------------------------")

/*
2.
    Напишіть програму, яка запитує у користувача місяць року
    (українською мовою маленкими літерами) та виводить повідомлення,
    скільки днів у цьому місяці. Результат виводиться в консоль.
    Скільки днів в кожному місяці можна знайти тут в розділі Юліанський і Григоріанський календарі - https://uk.wikipedia.org/wiki/%D0%9C%D1%96%D1%81%D1%8F%D1%86%D1%8C_(%D1%87%D0%B0%D1%81)
    (Використайте switch case)
 */

let userMonth =  prompt("Напишіть місяць року з маленькою літери -> ");

switch (userMonth){

    case 'січень': {
        alert("січень -> 31 днів");
        break;
    }

    case 'лютий': {
        alert("лютий -> 28 днів");
        break;
    }

    case 'березень': {
        alert("березень -> 31 днів");
        break;
    }

    case 'квітень': {
        alert("квітень -> 30 днів");
        break;
    }

    case 'травень': {
        alert("трутень -> 30 днів");
        break;
    }

    case 'червень': {
        alert("червень -> 30 днів");
        break;
    }

    case 'липень': {
        alert("липень -> 30 днів");
        break;
    }

    case 'серпень': {
        alert("серпень -> 31 днів");
        break;
    }

    case 'вересень': {
        alert("вересень -> 30 днів");
        break;
    }

    case 'жовтень': {
        alert("вересень -> 31 днів");
        break;
    }

    case 'листопад': {
        alert("листопад -> 31 днів");
        break;
    }

    case 'грудень': {
        alert("грудень -> 31 днів");
        break;
    }

    default: {
        alert("Wrong month")
    }
}


